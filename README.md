# 코딩테스트 합격자 되기 Python


![book-image](https://gitlab.com/devchaeyoung/coding-test-for-python/uploads/364182671a572ae4cb88932d22f5b96f/%E1%84%8F%E1%85%A9%E1%84%83%E1%85%B5%E1%86%BC%E1%84%90%E1%85%A6%E1%84%89%E1%85%B3%E1%84%90%E1%85%B3_%E1%84%92%E1%85%A1%E1%86%B8%E1%84%80%E1%85%A7%E1%86%A8%E1%84%8C%E1%85%A1%E1%84%83%E1%85%AC%E1%84%80%E1%85%B5_%E1%84%80%E1%85%AD%E1%84%8C%E1%85%A2.png)
- 저자 : 박경록, 출판사 : 골든래빗
- [책 맛보기 링크 ](https://wikidocs.net/book/13314)


## 완독 스터디 기간

> 묘공단 : 저자와 함께하는 완독 스터디

2024.01.21 ~
